package edu.usfca.cs.dfs;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

public class Controller {

    public static final String nodeMapHostNameKey = "hostName";
    public static final String nodeMapPortNumKey = "portNum";
    public static final String controllerHostName = "localhost";
    public static final int controllerPort = 3001;
    public static final int maxNodesForOneChunk = 3;
    public static final int heartBeatConfirmedSeconds = 10;

    private static ArrayList <Map> activeNodes = new ArrayList<>();
    private static ArrayList <Map> heartBeatingNodes = new ArrayList<>();
    private static Map<String, Map> filesMap = new HashMap<>();

    public static void main(String[] args) throws Exception {

        System.out.println("Starting controller...");
        Controller controller = new Controller();
        CheckHeartBeatThread checkHeartBeatThread = controller.new CheckHeartBeatThread();
        checkHeartBeatThread.start();
        controller.clientServer();
    }


    private static List<Map> getActiveNodes() {
        Collections.shuffle(activeNodes);
        if (activeNodes.size() < maxNodesForOneChunk){
            System.out.println("not enough active nodes");
            return null;
        }
        return activeNodes.subList(0, maxNodesForOneChunk);
    }


    public void clientServer() throws Exception {

        ServerSocket controllerServSock = new ServerSocket(controllerPort);
        System.out.println("Listening..." + controllerPort);

        while (true){
            Socket socket = controllerServSock.accept();

            StorageMessages.RequestToCtrllerWrapper requestToCtrllerWrapper
                    = StorageMessages.RequestToCtrllerWrapper.parseDelimitedFrom(
                    socket.getInputStream());

            if (requestToCtrllerWrapper.hasActiveNodesRequestMsg()){

                sendActiveNodes(socket, requestToCtrllerWrapper.getActiveNodesRequestMsg());

            } else if (requestToCtrllerWrapper.hasChunksMapRequestMsg()){

                sendChunksMap(socket, requestToCtrllerWrapper.getChunksMapRequestMsg());

            } else if (requestToCtrllerWrapper.hasHeartBeat()){

                receiveHeartBeat(requestToCtrllerWrapper.getHeartBeat());

            } else if (requestToCtrllerWrapper.hasFileListMsg()){

                sendFileList(socket);
            }
        }
    }


    private static void sendActiveNodes (Socket socket, StorageMessages.ActiveNodesRequest activeNodesRequest)
            throws Exception{

        List activeNodesList = getActiveNodes();
        List nodeMetaDataList = makeNodeMetaDataListInProto(activeNodesList);

        StorageMessages.ActiveNodesResponse activeNodesResponse = StorageMessages.ActiveNodesResponse.newBuilder()
                .addAllActiveNode(nodeMetaDataList)
                .build();
        activeNodesResponse.writeDelimitedTo(socket.getOutputStream());
    }


    private static void sendFileList (Socket socket) throws Exception {

        StorageMessages.FileListMsg fileListMsg = StorageMessages.FileListMsg.newBuilder()
                .addAllFileName(getKeyList(filesMap))
                .build();

        fileListMsg.writeDelimitedTo(socket.getOutputStream());
    }


    private static void receiveHeartBeat (StorageMessages.HeartBeat heartBeat) throws Exception {

        StorageMessages.NodeMetaData currentNode = heartBeat.getCurrentNode();
        Map nodeMetaDataMap = new HashMap();
        nodeMetaDataMap.put(nodeMapHostNameKey, currentNode.getHostName());
        nodeMetaDataMap.put(nodeMapPortNumKey, currentNode.getNode());

        if (!activeNodes.contains(nodeMetaDataMap)){
            System.out.println(currentNode.getNode() + " add to AN");
            activeNodes.add(nodeMetaDataMap);
        }

        if (!heartBeatingNodes.contains(nodeMetaDataMap)){
            System.out.println(currentNode.getNode() + " add to HBN");
            heartBeatingNodes.add(nodeMetaDataMap);
        }


        if (!heartBeat.getChunkNamesList().isEmpty()){

            updateFilesMap(nodeMetaDataMap, heartBeat.getChunkNamesList());
        }

        if (!heartBeat.getCorruptedChunkNamesList().isEmpty()){

            fixCorruptedChunks(heartBeat.getCorruptedChunkNamesList(), currentNode);
        }
        System.out.println("heartbeat received from port " + heartBeat.getCurrentNode().getNode());
        System.out.println("----------------------------------------------------");
    }


    private static void sendChunksMap (Socket socket, StorageMessages.ChunksMapRequest chunksMapRequest)
            throws Exception {

        Map <String, ArrayList> chunksMap = filesMap.get(chunksMapRequest.getFileName());
        HashMap <String, StorageMessages.ChunksLocArr>chunksMapInProto = makeChunksMapInProto(chunksMap);

        StorageMessages.ChunksMapResponse chunksMapResponse = StorageMessages.ChunksMapResponse.newBuilder()
                .putAllChunksMap(chunksMapInProto)
                .build();

        chunksMapResponse.writeDelimitedTo(socket.getOutputStream());
    }


    private static HashMap makeChunksMapInProto(Map<String, ArrayList> chunksMap) {

        HashMap <String, StorageMessages.ChunksLocArr>tmpMap = new HashMap<>();

        for (Map.Entry<String, ArrayList> entry : chunksMap.entrySet()){

            List chunksLocList = entry.getValue();
            List <StorageMessages.NodeMetaData> chunksLocListInProto = makeNodeMetaDataListInProto(chunksLocList);

            StorageMessages.ChunksLocArr chunksLocArr = StorageMessages.ChunksLocArr.newBuilder()
                    .addAllChunksLocNode(chunksLocListInProto)
                    .build();

            tmpMap.put(entry.getKey(), chunksLocArr);
        }
        return tmpMap;
    }


    private static List makeNodeMetaDataListInProto(List nodeList) {
        List <StorageMessages.NodeMetaData> nodeMetaDataList = new ArrayList<>();

        for (int i=0; i <nodeList.size(); i++){

            Map tmpNodeMap = (Map) nodeList.get(i);

            StorageMessages.NodeMetaData nodeMetaData = makeNodeMetaDataFromMap(tmpNodeMap);

            nodeMetaDataList.add(nodeMetaData);
        }
        return nodeMetaDataList;
    }


    private static StorageMessages.NodeMetaData makeNodeMetaDataFromMap(Map map) {
        StorageMessages.NodeMetaData nodeMetaData = StorageMessages.NodeMetaData.newBuilder()
                .setNode((int) map.get(nodeMapPortNumKey))
                .setHostName((String) map.get(nodeMapHostNameKey))
                .build();
        return nodeMetaData;
    }


    private static List getKeyList(Map map) {
        return new ArrayList<>(map.keySet());
    }


    private static void updateFilesMap(Map nodeMetaDataMap, List<String>chunkNamesList) {
        System.out.println("---------------------------------------");
        System.out.println("heartbeat update chunkNames");
        System.out.println(chunkNamesList);

        for (String chunkName:chunkNamesList) {
            String fileName = StorageNode.getFileNameFromChunkName(chunkName);

            Map <String, List>chunksMap = new HashMap<>();
            List <Map>tmpList = new ArrayList<>();

            if (filesMap.containsKey(fileName)){
                chunksMap = filesMap.get(fileName);
                if (chunksMap.containsKey(chunkName)){
                    tmpList = chunksMap.get(chunkName);
                }
            }

            tmpList.add(nodeMetaDataMap);
            chunksMap.put(chunkName, tmpList);
            filesMap.put(fileName, chunksMap);
        }
    }


    private static void fixCorruptedChunks(List<String> corruptedChunkNamesList, StorageMessages.NodeMetaData heartBeatNode) throws IOException{
        for (String chunkName:corruptedChunkNamesList) {
            System.out.println(chunkName + " corrupted");

            String fileName = StorageNode.getFileNameFromChunkName(chunkName);
            Map chunksMap = filesMap.get(fileName);
            List <Map>chunksLocList = (List)chunksMap.get(chunkName);

            for (Map nodeMetaDataMap:chunksLocList) {

                StorageMessages.NodeMetaData node = makeNodeMetaDataFromMap(nodeMetaDataMap);

                Socket sock = new Socket(node.getHostName(), node.getNode());

                StorageMessages.RecoverChunk recoverChunk = StorageMessages.RecoverChunk.newBuilder()
                        .setChunkName(chunkName)
                        .setCorruptedNode(heartBeatNode)
                        .setCurrentNode(node)
                        .build();
                StorageMessages.StorageMessageWrapper msgWrapper = StorageMessages.StorageMessageWrapper.newBuilder()
                        .setRecoverChunk(recoverChunk)
                        .build();

                msgWrapper.writeDelimitedTo(sock.getOutputStream());
                sock.close();
            }
        }
    }

    class CheckHeartBeatThread extends Thread {
        public void run() {
            Timer timer = new Timer();
            timer.schedule(new CheckHeartBeat(), 0, heartBeatConfirmedSeconds*1000);
        }
    }


    class CheckHeartBeat extends TimerTask {
        public void run(){

            List<Map> downNodes = (List) activeNodes.clone();
            downNodes.removeAll(heartBeatingNodes);
            System.out.println(downNodes + " died nodes");

            activeNodes.retainAll(heartBeatingNodes);
            heartBeatingNodes.clear();

            if (!downNodes.isEmpty()){

                for (Map downNode:downNodes) {

                    for (Map.Entry<String, Map> filesMapEntry : filesMap.entrySet()){

                        Map <String, List>chunksMapEntry = filesMapEntry.getValue();

                        for (Map.Entry<String, List> chunksLocListEntry : chunksMapEntry.entrySet()){

                            checkIfDownNodeInChunkLoc(chunksLocListEntry, downNodes, downNode);
                        }
                    }
                    try {
                        System.out.println("wait " + Integer.toString(StorageNode.heartBeatSeconds+1) + " seconds for previous recovering node info update DS with heartbeat");
                        Thread.sleep((StorageNode.heartBeatSeconds + 1)*1000);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                    System.out.println("replica for node: " + downNode + " done");
                    System.out.println("--------------------------------------------------");
                }
            }
        }
    }


    private static void checkIfDownNodeInChunkLoc(Map.Entry<String, List> chunksLocListEntry, List<Map> downNodes, Map downNode) {
        if (chunksLocListEntry.getValue().contains(downNode)){
            try {
                recoverChunkToOtherActiveNode(chunksLocListEntry, downNodes, downNode);
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }


    private static void recoverChunkToOtherActiveNode(Map.Entry<String, List> chunksLocListEntry, List<Map> downNodes, Map downNode) throws IOException{
        String chunkName = chunksLocListEntry.getKey();

        chunksLocListEntry.getValue().remove(downNode);

        Map recoverFromNodeMap = (Map) chunksLocListEntry.getValue().get(0);

        while (downNodes.contains(recoverFromNodeMap)){
            System.out.println("recoverFromNode is down, shuffle to get other node....");
            Collections.shuffle(chunksLocListEntry.getValue());
            recoverFromNodeMap = (Map) chunksLocListEntry.getValue().get(0);
        }

        Map recoverToNodeMap = getActiveNodes().get(0);

        while (chunksLocListEntry.getValue().contains(recoverToNodeMap)){
            System.out.println("recoverToNode is in the chunkLocList, shuffling for new node.....");
            recoverToNodeMap = getActiveNodes().get(0);
        }

        //todo, recover chunk with good Node using pipeline

        StorageMessages.NodeMetaData recoverFromNode = makeNodeMetaDataFromMap(recoverFromNodeMap);

        StorageMessages.NodeMetaData recoverToNode = makeNodeMetaDataFromMap(recoverToNodeMap);

        Socket sock = new Socket(recoverFromNode.getHostName(), recoverFromNode.getNode());

        StorageMessages.RecoverChunk recoverChunk = StorageMessages.RecoverChunk.newBuilder()
                .setChunkName(chunkName)
                .setCorruptedNode(recoverToNode)
                .setCurrentNode(recoverFromNode)
                .build();
        StorageMessages.StorageMessageWrapper msgWrapper = StorageMessages.StorageMessageWrapper.newBuilder()
                .setRecoverChunk(recoverChunk)
                .build();

        msgWrapper.writeDelimitedTo(sock.getOutputStream());
        sock.close();

        System.out.println("recover " + chunkName + " from " + recoverFromNode.getNode() + " to " + recoverToNode.getNode());
    }

//    private static Long getFreeSpaceInActiveNodes() throws IOException{
//        for (Map nodeMap:activeNodes) {
//            Socket sock = new Socket((String) nodeMap.get(nodeMapHostNameKey),(int)nodeMap.get(nodeMapPortNumKey));
//            //todo, msg for retreive freespaces.
//            sock.close();
//        }
//        return null;
//    }
}
