package edu.usfca.cs.dfs;

import com.google.protobuf.ByteString;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.util.*;

public class StorageNode {

    private ServerSocket srvSocket;

    public static final int portNum = 9996;
    public static final String controllerHostName = Controller.controllerHostName;
    public static final int controllerPort = Controller.controllerPort;
    private static final String thisHostName = "localhost";
    private static final String checkSumType = "MD5";
    private static final String outputFilePath = "port" + Integer.toString(portNum) + "/";
    public static final int heartBeatSeconds = 5;

    private static Map<String, String> chunksMap = new HashMap<>();
    private static Map<String, String> checkSumMap = new HashMap<>();
    private static List<String> chunksNameList = new ArrayList<>();
    private static List<String> corruptedChunkNames = new ArrayList<>();

    public static void main(String[] args)
            throws Exception {
        String hostname = getHostname();
        System.out.println("Starting storage node on " + hostname + "...");

        createDirIfNotExist(outputFilePath);

        for(File file: new java.io.File(outputFilePath).listFiles()){
            if (!file.isDirectory()){
                file.delete();
            }
        }

        StorageNode node = new StorageNode();
        HeartBeatThread thread = node.new HeartBeatThread();

        thread.start();
        node.start();
    }


    public void start()
            throws Exception {
        srvSocket = new ServerSocket(portNum);
        System.out.println("Listening..." + portNum);
        while (true) {

            Socket socket = srvSocket.accept();

            StorageMessages.StorageMessageWrapper msgWrapper
                    = StorageMessages.StorageMessageWrapper.parseDelimitedFrom(
                    socket.getInputStream());

            Boolean recoveringChunkOrNot;

            if (msgWrapper.hasStoreChunkMsg()) {

                storeChunk(msgWrapper.getStoreChunkMsg());

            } else if (msgWrapper.hasRetrieveFileMsg()) {

                recoveringChunkOrNot = false;
                ByteString chunkData = retrieveChunk(msgWrapper.getRetrieveFileMsg().getFileName(), msgWrapper.getRetrieveFileMsg().getChunkId(), recoveringChunkOrNot);

                sendChunkData(socket, chunkData);

            } else if (msgWrapper.hasRecoverChunk()) {

                recoveringChunkOrNot = true;

                StorageMessages.RecoverChunk recoverChunk = msgWrapper.getRecoverChunk();
                String chunkName = recoverChunk.getChunkName();

                String fileName = getFileNameFromChunkName(chunkName);
                int chunkId = getChunkIdFromChunkName(chunkName);
                ByteString chunkData = retrieveChunk(fileName, chunkId, recoveringChunkOrNot);

                if (chunkData != null){

                    recoverChunkUsingPipeline(recoverChunk, fileName, chunkId, chunkData);
                }

            }
        }
    }

    /**
     * Retrieves the short host name of the current host.
     *
     * @return name of the current host
     */
    private static String getHostname()
            throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }


    private static void storeChunk(StorageMessages.StoreChunk storeChunkMsg) throws Exception {

        String fileName = storeChunkMsg.getFileName();
        int chunkId = storeChunkMsg.getChunkId();
        ByteString chunkData = storeChunkMsg.getData();
        StorageMessages.ActiveNodesArr activeNodesArr = storeChunkMsg.getActiveNodes();

        String chunkName = makeChunkName(fileName, chunkId);
        String chunkCheckSumName = makeCheckSumName(chunkName);

        String path = outputFilePath;
        String pathToChunk = path + chunkName;
        String pathToCheckSum = path + chunkCheckSumName;

        writeBinaryFile(pathToChunk, chunkData);
        writeFile(pathToCheckSum, getChecksum(pathToChunk, chunkData.size()));

        chunksMap.put(chunkName, pathToChunk);
        checkSumMap.put(chunkCheckSumName, pathToCheckSum);

        chunksNameList.add(chunkName);

        System.out.println("storing " + chunkName + " in port " + portNum);

        System.out.println("--------------------------------------------------------------------------");

        if (activeNodesArr.getActiveNodeCount()-1 > 0) {
            sendChunkPipeline(storeChunkMsg);
        }

    }


    private static ByteString retrieveChunk(String fileName, int chunkId, Boolean recoveringChunkOrNot) throws Exception{

        String chunkName = makeChunkName(fileName, chunkId);
        String chunkPath = chunksMap.get(chunkName);
//        System.out.println(chunksMap);
        System.out.println("******************************* retrieving " + chunkName);
        ByteString chunkData = readBinaryFile(chunkPath);


        String checkSumName = makeCheckSumName(chunkName);
        String checkSumPath = checkSumMap.get(checkSumName);

        String checkSum = readFile(checkSumPath);
        String newCheckSum = getChecksum(chunkPath, chunkData.size());

        System.out.println(chunkName + " - checkSum in disk " + checkSum);
        System.out.println(chunkName + " - new checkSum " + newCheckSum);

        if (checkSum.equals(newCheckSum)){
            System.out.println("checkSum same");
        } else {
            System.out.println("checkSum different");
            if (!recoveringChunkOrNot){
                corruptedChunkNames.add(chunkName);
            }
            return null;
        }

        System.out.println("--------------------------------------------------------------------------");
        return chunkData;
    }


    private static void sendChunkPipeline(StorageMessages.StoreChunk storeChunkMsg) throws Exception {

        String fileName = storeChunkMsg.getFileName();
        int chunkId = storeChunkMsg.getChunkId();
        ByteString chunkData = storeChunkMsg.getData();
        StorageMessages.ActiveNodesArr activeNodesArr = storeChunkMsg.getActiveNodes();

        List tmpList = activeNodesArr.getActiveNodeList().subList(1, activeNodesArr.getActiveNodeCount());

        StorageMessages.ActiveNodesArr newActiveNodesArr = StorageMessages.ActiveNodesArr.newBuilder()
                .addAllActiveNode(tmpList)
                .build();

        String hostName = newActiveNodesArr.getActiveNode(0).getHostName();
        int portNum = newActiveNodesArr.getActiveNode(0).getNode();

        Socket sock = new Socket(hostName, portNum);

        storeChunkMsg = StorageMessages.StoreChunk.newBuilder()
                .setFileName(fileName)
                .setChunkId(chunkId)
                .setData(chunkData)
                .setActiveNodes(newActiveNodesArr)
                .build();

        StorageMessages.StorageMessageWrapper msgWrapper = StorageMessages.StorageMessageWrapper.newBuilder()
                .setStoreChunkMsg(storeChunkMsg)
                .build();

        msgWrapper.writeDelimitedTo(sock.getOutputStream());
        sock.close();
    }


    private static void sendChunkData(Socket socket, ByteString chunkData) throws IOException{
        StorageMessages.RetrieveChunkResponse rtrvChunkResponse;
        if (chunkData != null){
            rtrvChunkResponse = StorageMessages.RetrieveChunkResponse.newBuilder()
                    .setChunkData(chunkData)
                    .build();
        }else {
            rtrvChunkResponse = StorageMessages.RetrieveChunkResponse.newBuilder()
                    .build();
        }
        rtrvChunkResponse.writeDelimitedTo(socket.getOutputStream());
    }


    private static void recoverChunkUsingPipeline(StorageMessages.RecoverChunk recoverChunk, String fileName, int chunkId, ByteString chunkData) throws Exception{
        StorageMessages.NodeMetaData corruptedNode = recoverChunk.getCorruptedNode();
        StorageMessages.NodeMetaData currentNode = recoverChunk.getCurrentNode();

        List <StorageMessages.NodeMetaData>tmpList = new ArrayList<>();
        tmpList.add(currentNode);
        tmpList.add(corruptedNode);

        StorageMessages.ActiveNodesArr activeNodesArr = StorageMessages.ActiveNodesArr.newBuilder()
                .addAllActiveNode(tmpList)
                .build();

        StorageMessages.StoreChunk storeChunkMsg = StorageMessages.StoreChunk.newBuilder()
                .setFileName(fileName)
                .setChunkId(chunkId)
                .setData(chunkData)
                .setActiveNodes(activeNodesArr)
                .build();

        sendChunkPipeline(storeChunkMsg);
        System.out.println("recover " + makeChunkName(fileName, chunkId) + " to " + corruptedNode.getNode() + " from " + currentNode.getNode());
        System.out.println("----------------------------------------------------------------");
    }


    public static String getChecksum(String pathToFile, int size) {

        StringBuffer sb = new StringBuffer("");
        try {
            MessageDigest md = MessageDigest.getInstance(checkSumType);
            FileInputStream fis = new FileInputStream(pathToFile);
            byte[] dataBytes = new byte[size];
            int nread;

            while ((nread = fis.read(dataBytes)) != -1) {
                md.update(dataBytes, 0, nread);
            }
            byte[] mdbytes = md.digest();

            //convert the byte to hex format
            for (int i = 0; i < mdbytes.length; i++) {
                sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return sb.toString();
    }


    private static void writeFile(String filePath, String data) throws Exception{
        File chunkFile = new File(filePath);
        FileWriter fw = new FileWriter(chunkFile);
        fw.write(data);
        fw.close();
    }

    public static void writeBinaryFile(String filePath, ByteString data) throws Exception{
        FileOutputStream output = new FileOutputStream(filePath);
        output.write(data.toByteArray());
        output.close();
    }


    public static String readFile(String filePath) {
        String line;
        String result = "";

        try {
            FileReader fr = new FileReader(filePath);
            BufferedReader br = new BufferedReader(fr);
            while ((line = br.readLine()) != null) {
                result +=line;
            }
        }catch(FileNotFoundException ex) {
            System.out.println(
                    "Unable to open file '" +
                            filePath + "'");
        }
        catch(IOException ex) {
            System.out.println(
                    "Error reading file '"
                            + filePath + "'");
        }
        return result;
    }

    public static ByteString readBinaryFile(String filePath) throws IOException{
        Path path = Paths.get(filePath);
        ByteString result = ByteString.copyFrom(Files.readAllBytes(path));
        return result;
    }


//    private static Long getFreeSpace() {
//        File file = new File(outputFilePath);
//        long freeSpace = file.getFreeSpace(); //unallocated / free disk space in bytes.
//        return freeSpace;
//    }

    public static String makeChunkName(String fileName, int chunkId) {
        return fileName + "_" + chunkId;
    }


    private static String makeCheckSumName(String chunkName) {
        return chunkName + "_" + "checksum.txt";
    }

    class HeartBeatThread extends Thread {
        public void run() {
            Timer timer = new Timer();
            timer.schedule(new HeartBeat(), 0, heartBeatSeconds*1000);
        }
    }


    public static String getFileNameFromChunkName(String chunkName) {
        int underscoreIndex = chunkName.lastIndexOf("_");
        return chunkName.substring(0, underscoreIndex);
    }


    public static int getChunkIdFromChunkName(String chunkName) {
        int underscoreIndex = chunkName.lastIndexOf("_");
        return Integer.valueOf(chunkName.substring(underscoreIndex+1));
    }


    class HeartBeat extends TimerTask {
        public void run(){
            try{
                Socket sock = new Socket(controllerHostName, controllerPort);

                StorageMessages.NodeMetaData nodeMetaData = StorageMessages.NodeMetaData.newBuilder()
                        .setHostName(thisHostName)
                        .setNode(portNum)
                        .build();

                StorageMessages.HeartBeat heartBeat;

                if (chunksNameList.isEmpty() && corruptedChunkNames.isEmpty()){
                    heartBeat = StorageMessages.HeartBeat.newBuilder()
                            .setCurrentNode(nodeMetaData)
                            .build();
                } else if (corruptedChunkNames.isEmpty()) {
                    heartBeat = StorageMessages.HeartBeat.newBuilder()
                            .setCurrentNode(nodeMetaData)
                            .addAllChunkNames(chunksNameList)
                            .build();
                } else if (chunksNameList.isEmpty()) {
                    heartBeat = StorageMessages.HeartBeat.newBuilder()
                            .setCurrentNode(nodeMetaData)
                            .addAllCorruptedChunkNames(corruptedChunkNames)
                            .build();
                } else {
                    heartBeat = StorageMessages.HeartBeat.newBuilder()
                            .setCurrentNode(nodeMetaData)
                            .addAllChunkNames(chunksNameList)
                            .addAllCorruptedChunkNames(corruptedChunkNames)
                            .build();
                }

                StorageMessages.RequestToCtrllerWrapper requestToCtrllerWrapper = StorageMessages.RequestToCtrllerWrapper.newBuilder()
                        .setHeartBeat(heartBeat)
                        .build();

                requestToCtrllerWrapper.writeDelimitedTo(sock.getOutputStream());
                sock.close();
                System.out.println("beating in " + portNum);
//                chunksNameList = new ArrayList<>();
//                corruptedChunkNames = new ArrayList<>();
                chunksNameList.clear();
                corruptedChunkNames.clear();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    public static void createDirIfNotExist(String path) {
        File theDir = new File(path);

        // if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            boolean result = false;

            try{
                theDir.mkdir();
                result = true;
            }
            catch(SecurityException se){
                se.printStackTrace();
            }
            if(result) {
                System.out.println("DIR created");
            }
        }
    }
}
