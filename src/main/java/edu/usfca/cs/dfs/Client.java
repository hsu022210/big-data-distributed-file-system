package edu.usfca.cs.dfs;

import java.io.File;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

import com.google.protobuf.ByteString;


public class Client {

    private static final String controllerHostName = Controller.controllerHostName;
    private static final int controllerPort = Controller.controllerPort;
    private static final int fixChunkSize = 256;
    private static final int chunkIdStartFrom = 1;
    private static final int fixChunkAmounts = 64;
    private static final String inputFilePath = "input/";
    private static final String outputFilePath = "output/";

    public static void main(String[] args)
    throws Exception {
        String fileName = "test_file_1.bin";
        ByteString inputData = StorageNode.readBinaryFile(inputFilePath + fileName);

        saveFile(fileName, inputData);

        System.out.println(fileName + " saved!");

        Thread.sleep(8000);

        ByteString outputData = getFile(fileName);

        System.out.println("*****************************************************");

        if (inputData.equals(outputData)){
            System.out.println(fileName + " data are the same");
        }else {
            System.out.println(fileName + " data not the same");
        }

        StorageNode.createDirIfNotExist(outputFilePath);

        StorageNode.writeBinaryFile(outputFilePath + "output_" + fileName, outputData);
        String inputDataCheckSum = StorageNode.getChecksum(inputFilePath + fileName, inputData.size());
        String outputDataCheckSum = StorageNode.getChecksum(outputFilePath + "output_" + fileName, outputData.size());
        System.out.println("input file " + fileName + " checkSum: " + inputDataCheckSum);
        System.out.println("output file " + "output_" + fileName + " checkSum: " + outputDataCheckSum);

        if (inputDataCheckSum.equals(outputDataCheckSum)){
            System.out.println("file checkSum matched");
        }else {
            System.out.println("file checkSum different");
        }
        System.out.println("files in DFS: " + getFileList());
    }


    private static void saveFile(String fileName, ByteString data) throws Exception{
        int chunkId = chunkIdStartFrom;

        int dataSize = data.size();
        int totalSize = 0;
//        int chunkSize = fixChunkSize;
        Long L = divide(dataSize, fixChunkAmounts);
        int chunkSize = L.intValue();
        System.out.println(dataSize + " datasize");

        ByteString chunkData;

        int startIndex = 0;
        int endIndex;

        while (totalSize < dataSize){

            List activeNodes = getActiveNodes(fileName, chunkId);
            System.out.println("store " + fileName + "_" + chunkId + " pipeline " + activeNodes.toString());

            endIndex = startIndex + chunkSize;

            if (endIndex > dataSize){
                endIndex = dataSize;
            }

            chunkData = data.substring(startIndex, endIndex);
            startIndex = endIndex;
            totalSize = endIndex;

            storeChunkToNode(activeNodes, fileName, chunkId, chunkData);

            chunkId += 1;
        }
        System.out.println(totalSize + " totalsize");
    }


    private static long divide(long num, long divisor) {
        return num / divisor;
    }


    private static void storeChunkToNode(List activeNodes, String fileName, int chunkId, ByteString chunkData) throws Exception {

        StorageMessages.NodeMetaData firstActiveNode = (StorageMessages.NodeMetaData) activeNodes.get(0);

        String hostName = firstActiveNode.getHostName();
        int portNum = firstActiveNode.getNode();

        Socket sock = new Socket(hostName, portNum);

        StorageMessages.ActiveNodesArr activeNodesArr = StorageMessages.ActiveNodesArr.newBuilder()
                .addAllActiveNode(activeNodes)
                .build();

        StorageMessages.StoreChunk storeChunkMsg = StorageMessages.StoreChunk.newBuilder()
                .setFileName(fileName)
                .setChunkId(chunkId)
                .setData(chunkData)
                .setActiveNodes(activeNodesArr)
                .build();

        StorageMessages.StorageMessageWrapper msgWrapper = StorageMessages.StorageMessageWrapper.newBuilder()
                .setStoreChunkMsg(storeChunkMsg)
                .build();

        msgWrapper.writeDelimitedTo(sock.getOutputStream());
        sock.close();
    }


    private static ByteString getFile(String fileName) throws Exception{

        Map <String, StorageMessages.ChunksLocArr> chunksMap = getChunksMap(fileName);

        ByteString result = ByteString.EMPTY;

        for (int chunkId=chunkIdStartFrom; chunkId<=chunksMap.size(); chunkId++){
            String chunkName = StorageNode.makeChunkName(fileName, chunkId);
            StorageMessages.ChunksLocArr chunksLocArr = chunksMap.get(chunkName);
            System.out.println("----------------------------------------------------------");

            System.out.println(chunkName + " ---locations: " + chunksLocArr.getChunksLocNodeList().toString());

            int nodeIndex = 0;
            StorageMessages.NodeMetaData firstNode = chunksLocArr.getChunksLocNode(nodeIndex);

            ByteString chunkData = getChunkData(firstNode, fileName, chunkId);

            while (chunkData.isEmpty()){
                nodeIndex += 1;
                if (nodeIndex >= Controller.maxNodesForOneChunk){
                    System.out.println("all chunks data are corrupted!");
                    return null;
                }
                StorageMessages.NodeMetaData nextNode = chunksLocArr.getChunksLocNode(nodeIndex);
                chunkData = getChunkData(nextNode, fileName, chunkId);
            }
            result = result.concat(chunkData);

        }
        return result;
    }


    private static Map getChunksMap(String fileName) throws Exception{

        Socket sockChunksMap = new Socket(controllerHostName, controllerPort);
        StorageMessages.ChunksMapRequest chunksMapRequest =  StorageMessages.ChunksMapRequest.newBuilder()
                .setRequest(true)
                .setFileName(fileName)
                .build();

        StorageMessages.RequestToCtrllerWrapper requestToCtrllerWrapper = StorageMessages.RequestToCtrllerWrapper.newBuilder()
                .setChunksMapRequestMsg(chunksMapRequest)
                .build();

        requestToCtrllerWrapper.writeDelimitedTo(sockChunksMap.getOutputStream());

        StorageMessages.ChunksMapResponse chunksMapResponse
                = StorageMessages.ChunksMapResponse.parseDelimitedFrom(
                sockChunksMap.getInputStream());
        Map <String, StorageMessages.ChunksLocArr> chunksMap = chunksMapResponse.getChunksMapMap();

        sockChunksMap.close();
        return chunksMap;

    }


    private static ByteString getChunkData(StorageMessages.NodeMetaData node, String fileName, int chunkId) throws Exception{
        int portNum = node.getNode();
        String hostName = node.getHostName();

        System.out.println("retrieve " + fileName + "_" + chunkId + " from " + portNum);

        Socket sock = new Socket(hostName, portNum);
        StorageMessages.RetrieveFile retrieveFile = StorageMessages.RetrieveFile.newBuilder()
                .setFileName(fileName)
                .setChunkId(chunkId)
                .build();

        StorageMessages.StorageMessageWrapper msgWrapper = StorageMessages.StorageMessageWrapper.newBuilder()
                .setRetrieveFileMsg(retrieveFile)
                .build();

        msgWrapper.writeDelimitedTo(sock.getOutputStream());

        StorageMessages.RetrieveChunkResponse rtrvChunkResponse
                = StorageMessages.RetrieveChunkResponse.parseDelimitedFrom(
                sock.getInputStream());
        sock.close();
        return rtrvChunkResponse.getChunkData();
    }


    private static List getActiveNodes(String fileName, int chunkId) throws Exception{
        Socket sock = new Socket(controllerHostName, controllerPort);

        StorageMessages.ActiveNodesRequest activeNodesRequest = StorageMessages.ActiveNodesRequest.newBuilder()
                .setRequest(true)
                .setFileName(fileName)
                .setChunkId(chunkId)
                .build();

        StorageMessages.RequestToCtrllerWrapper requestToCtrllerWrapper = StorageMessages.RequestToCtrllerWrapper.newBuilder()
                .setActiveNodesRequestMsg(activeNodesRequest)
                .build();

        requestToCtrllerWrapper.writeDelimitedTo(sock.getOutputStream());

        StorageMessages.ActiveNodesResponse activeNodesResponse
                = StorageMessages.ActiveNodesResponse.parseDelimitedFrom(
                sock.getInputStream());

        List result = activeNodesResponse.getActiveNodeList();

        sock.close();
        return result;
    }


    private static List getFileList() throws Exception{
        Socket sock = new Socket(controllerHostName, controllerPort);

        StorageMessages.FileListMsg fileListMsg = StorageMessages.FileListMsg.newBuilder()
                .build();

        StorageMessages.RequestToCtrllerWrapper requestToCtrllerWrapper = StorageMessages.RequestToCtrllerWrapper.newBuilder()
                .setFileListMsg(fileListMsg)
                .build();

        requestToCtrllerWrapper.writeDelimitedTo(sock.getOutputStream());

        fileListMsg = StorageMessages.FileListMsg.parseDelimitedFrom(
                sock.getInputStream());

        List result = fileListMsg.getFileNameList();
        sock.close();

        return result;
    }
}
